# Sidebar
notes = Notes
sync = Sync

# search page
range = range is from
to = to
search = Type your search text ...
tags = tags:

# sync page 
sync-waiting = Waiting for your operating...
sync-syncing = Attempting to sync...
sync-error = Synchronization process error:
sync-complete = Synchronization process was complete！
sync-ip-parse-error = Your IP address input format error,please check it.
sync-ip-parse-complete = Cool! Your up address format was passed.
sync-file-path-error = The selected file path is incorrect. Please select the file again.
sync-from-file-unknown-error = An unknown error occurred during file synchronization. The file you want to synchronize may not be the correct file.

sync-client-tip = Enter the IP address and port number, and your device will connect to the synchronization service of other devices.
sync-server-tip = After enabling the synchronization service, your device will be able to be synchronized by Local Native on other devices
input-ip-tip = If you want to enter the IPv6 address, you can copy it and paste it into the ip input box via keyboard shortcuts.
input-ip = IP address and port number:
sync-from-server = Sync from server to local
sync-to-server = Sync local to server
sync-from-file = Sync from file to local

closed = Local service is off
opened = Local service is on
starting = Starting local service
closing = Closing local service
unknow-error = Unknown error click to try again
ip-qr = Use the Local Native app on the device that needs to be synchronized, scan the QR code to synchronize, or manually enter: {$ip} to synchronize.

settings = Settings
disable-delete-tip = When deleting, delete directly instead of warning.
language = Language
chinese = Chinese(简体中文)
english = English
limit = Maximum number of results per page search
ok = Ok
cancel = Cancel

delete-tip = Warning
delete-tip-content = It cannot be restored after deletion, please operate carefully!

not-found = Sorry, the result you want was not found!
nothing = You have not yet created a label, you can sync from other devices to this device, or add a new label from the browser extension.

try-fix-host = Your browser can’t communicate normally? Try clicking here to fix it.

sync-file-title = Select the sqlite3 file you will use for synchronization

sync-file = *.sqlite3 file for synchronization

date = Date
count = Count