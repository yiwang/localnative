﻿/*
    Local Native
    Copyright (C) 2018-2019  Yi Wang

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
let locales = {};

locales['zh-CN'] = i18n.create({
  values: {
    "ssbify": '保存内容到 ssb',
    "public": '公开',
    "language": '语言',
    "description": "描述",
    "title": '标题',
    "url": 'url 链接',
    "type to add tags, enter to save, comma or space as tag seperator": '输入标签，回车保存，半角逗号或空格为标签的分隔符',
    "type to search": '输入搜索, 空格分隔单词',
    "clear search term(s)": '清除搜索框',
    "prev": '上页',
    "next": '下页',
    "page": '页',
    "of": '共',
    "ssb sync": 'ssb 同步'
  }
});
